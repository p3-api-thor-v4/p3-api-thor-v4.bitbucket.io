/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9828913879077499, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.931786941580756, 500, 1500, "getDashboardData"], "isController": false}, {"data": [0.005628517823639775, 500, 1500, "getChildCheckInCheckOutByArea"], "isController": false}, {"data": [0.9971590909090909, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (health_check_type)"], "isController": false}, {"data": [0.9990545821252994, 500, 1500, "getAllEvents"], "isController": false}, {"data": [0.9980821917808219, 500, 1500, "addOrUpdateUserDevice"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.9957805907172996, 500, 1500, "findAllConfigByCategory (Relation_Child)"], "isController": false}, {"data": [0.9921011058451816, 500, 1500, "me"], "isController": false}, {"data": [0.9991285945474916, 500, 1500, "getNotificationCount"], "isController": false}, {"data": [0.9943181818181818, 500, 1500, "dismissChildCheckInByVPS"], "isController": false}, {"data": [0.9962121212121212, 500, 1500, "addClassesToArea"], "isController": false}, {"data": [0.9957805907172996, 500, 1500, "findAllConfigByCategory (non_parent_relationship)"], "isController": false}, {"data": [0.9978902953586498, 500, 1500, "findAllConfigByCategory (guardian_rejection_reason)"], "isController": false}, {"data": [0.37184873949579833, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9985248017702378, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [1.0, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.9991782322863404, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}, {"data": [0.9984877126654065, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.9997260774287802, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkout_decline_reason)"], "isController": false}, {"data": [0.9957805907172996, 500, 1500, "getAllCentreClasses"], "isController": false}, {"data": [0.9962121212121212, 500, 1500, "getAllArea"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkin_decline_reason)"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 77008, 0, 0.0, 97.35331394140788, 2, 16893, 17.0, 78.0, 257.0, 2211.9900000000016, 253.86862180142285, 1162.2883278615768, 263.27869445037385], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllConfigByCategory", 633, 0, 0.0, 7.083728278041073, 2, 306, 4.0, 10.0, 16.299999999999955, 71.47999999999911, 2.126073112732256, 1.9730608714380717, 1.8931894654840593], "isController": false}, {"data": ["getDashboardData", 2910, 0, 0.0, 366.2721649484543, 107, 1956, 344.5, 519.0, 589.0, 1057.229999999999, 9.729675510306434, 84.46004454210342, 25.90146039169467], "isController": false}, {"data": ["getChildCheckInCheckOutByArea", 533, 0, 0.0, 2527.5196998123847, 1190, 4218, 2548.0, 3120.4, 3259.6, 3595.859999999999, 1.767235519776128, 9.529184978663864, 5.867774186756675], "isController": false}, {"data": ["getAllClassInfo", 528, 0, 0.0, 47.50568181818183, 8, 1511, 27.0, 87.10000000000002, 133.0, 360.2300000000005, 1.77964130790154, 1.9655999211295327, 1.7396688957123452], "isController": false}, {"data": ["findAllLevels", 237, 0, 0.0, 22.19831223628692, 5, 229, 11.0, 46.60000000000005, 75.59999999999997, 203.9000000000002, 0.8095008761053786, 0.5154243859577214, 0.6814353078152698], "isController": false}, {"data": ["findAllConfigByCategory (health_check_type)", 237, 0, 0.0, 22.19831223628692, 5, 328, 12.0, 48.80000000000007, 75.39999999999998, 203.72000000000003, 0.8095617094390796, 0.7977028953359682, 0.7779382051641156], "isController": false}, {"data": ["getAllEvents", 7933, 0, 0.0, 22.589688642380004, 6, 1212, 12.0, 39.0, 64.0, 201.65999999999985, 26.55148638788666, 13.146097264314976, 28.833254749345667], "isController": false}, {"data": ["addOrUpdateUserDevice", 10950, 0, 0.0, 34.78894977168944, 10, 1929, 20.0, 61.0, 90.0, 229.0, 36.63837091415245, 17.675151593350886, 45.10100529144234], "isController": false}, {"data": ["getHomefeed", 105, 0, 0.0, 14047.047619047626, 6270, 16893, 14220.0, 15657.4, 15984.2, 16887.42, 0.34743017480701077, 65.49771298400333, 1.0555227283443462], "isController": false}, {"data": ["findAllConfigByCategory (Relation_Child)", 237, 0, 0.0, 33.054852320675096, 5, 1319, 13.0, 73.0, 121.39999999999998, 440.9600000000014, 0.8098881534208378, 0.6469614350568802, 0.7758791782283612], "isController": false}, {"data": ["me", 633, 0, 0.0, 78.6666666666666, 24, 1141, 46.0, 146.60000000000002, 217.5999999999999, 641.0199999999969, 2.12365510230784, 3.6181180144579645, 4.868039777016214], "isController": false}, {"data": ["getNotificationCount", 8033, 0, 0.0, 24.955309348935653, 8, 1591, 14.0, 45.0, 73.0, 174.0, 26.885823108487124, 13.968025286831201, 28.97516521242913], "isController": false}, {"data": ["dismissChildCheckInByVPS", 528, 0, 0.0, 37.87310606060605, 6, 1571, 14.0, 59.0, 111.94999999999959, 460.3500000000031, 1.7814306102412016, 0.9898769699484802, 1.3604284543052927], "isController": false}, {"data": ["addClassesToArea", 528, 0, 0.0, 69.67803030303024, 13, 1048, 53.0, 115.0, 179.0, 359.7900000000054, 1.7792215231888502, 0.9747492915126416, 1.294453158960638], "isController": false}, {"data": ["findAllConfigByCategory (non_parent_relationship)", 237, 0, 0.0, 51.59071729957805, 5, 1298, 15.0, 108.80000000000007, 224.69999999999996, 604.1800000000009, 0.8100653179250022, 0.662133467874245, 0.7831686179157736], "isController": false}, {"data": ["findAllConfigByCategory (guardian_rejection_reason)", 237, 0, 0.0, 27.265822784810123, 5, 718, 11.0, 64.40000000000003, 97.1, 220.22000000000014, 0.8095395871689683, 0.7708018530173282, 0.784241475069938], "isController": false}, {"data": ["getCountCheckInOutChildren", 238, 0, 0.0, 1267.2352941176475, 558, 2078, 1237.5, 1745.8, 1809.6999999999998, 2051.429999999999, 0.8063122732246731, 0.5094570710706675, 0.940172709209238], "isController": false}, {"data": ["getCentreHolidaysOfYear", 10846, 0, 0.0, 35.07855430573478, 11, 1876, 20.0, 63.0, 97.0, 229.0, 36.290754323036566, 65.69076073272458, 36.00616757329421], "isController": false}, {"data": ["findAllChildrenByParent", 105, 0, 0.0, 53.38095238095237, 20, 472, 30.0, 96.80000000000001, 147.69999999999987, 470.13999999999993, 0.35475969254159984, 0.7621790269448433, 0.5435722242165723], "isController": false}, {"data": ["findAllSchoolConfig", 10952, 0, 0.0, 36.7726442658874, 8, 1236, 22.0, 67.0, 108.0, 249.46999999999935, 36.63329575900696, 798.9206649317808, 21.930578836260672], "isController": false}, {"data": ["getChildCheckInCheckOut", 242, 0, 0.0, 4686.289256198348, 2904, 6175, 4728.0, 5263.8, 5462.65, 5733.7, 0.8003333619954096, 54.56882324331787, 2.3517608264103393], "isController": false}, {"data": ["getClassAttendanceSummaries", 7935, 0, 0.0, 26.566099558916182, 6, 1830, 12.0, 51.0, 84.0, 234.28000000000065, 26.552403770541723, 15.039447448158397, 29.119481869451516], "isController": false}, {"data": ["getLatestMobileVersion", 10952, 0, 0.0, 16.51616143170196, 9, 896, 13.0, 22.0, 29.0, 76.0, 36.526147278548564, 19.851365389666157, 21.442497196421428], "isController": false}, {"data": ["findAllConfigByCategory (checkout_decline_reason)", 237, 0, 0.0, 21.49367088607595, 5, 279, 12.0, 39.60000000000005, 78.1, 180.96000000000004, 0.8095008761053786, 0.5589034369204127, 0.7826229173284421], "isController": false}, {"data": ["getAllCentreClasses", 237, 0, 0.0, 46.45991561181433, 15, 1424, 26.0, 69.0, 93.59999999999997, 805.7800000000028, 0.8094676981402736, 1.1106465975459807, 0.9185561183974588], "isController": false}, {"data": ["getAllArea", 528, 0, 0.0, 55.65530303030299, 11, 1361, 30.5, 97.40000000000009, 171.44999999999914, 459.2800000000025, 1.7796173148675567, 3.300286407161612, 1.7500728867887008], "isController": false}, {"data": ["findAllConfigByCategory (checkin_decline_reason)", 237, 0, 0.0, 57.202531645569636, 5, 419, 17.0, 160.60000000000022, 247.19999999999982, 395.62000000000023, 0.8102425599562401, 0.5372604474709834, 0.7825487224577358], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 77008, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
